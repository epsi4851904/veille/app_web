import {useAuthStore} from "~/store/auth";
import {object} from "yup";

export default defineNuxtRouteMiddleware((to) => {
    const { authenticated, user } = storeToRefs(useAuthStore());
    const authStore = useAuthStore()
    const token = useCookie('token');
    const routePublic = ['index', 'appli', 'register', 'team', 'articles', 'login', 'article-id']
    const routeAuth = ['login', 'register']
    const routeAdmin = ['flux', 'flux-new', 'flux-id', 'users']

    if (!authenticated) {
        to.meta.layout = 'default'
    }

    if (token.value) {
        authenticated.value = true;
        to.meta.layout = 'auth';
        if (Object.keys(user.value).length === 0) {
            authStore.getUser().then(r => console.log())
        }
    }

    if (token.value && routeAdmin.includes(<string>to.name) && !user.value.isAdmin) {
        abortNavigation();
        return navigateTo('/feed');
    }

    if (token.value && routeAuth.includes(<string>to.name)) {
        abortNavigation();
        return navigateTo('/');
    }

    if (routePublic.includes(<string>to.name) && (to?.name !== 'login')) {
        return;
    }

    if (!token.value && !routePublic.includes(<string>to.name)) {
        abortNavigation();
        return navigateTo('/login');
    }
});