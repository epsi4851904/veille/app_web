// types/api.d.ts
import {T} from "@shikijs/core/chunk-tokens";
import type {UnwrapRef} from "vue";
import type {RouteParamValue} from "vue-router";

export interface Api {
    get<T = any>(url: string, options?: any): Promise<T>
    post<T = any>(url: string, body: any, options?: any): Promise<T>
    put<T = any>(url: string, body: any, options?: any): Promise<T>
    delete<T = any>(url: string, options?: any): Promise<T>
    postAuth<T = any>(url: string, body: any, options?: any): Promise<T>
    login<T = any>(data: { email: string, password: string }): Promise<T>
    register<T = any>(data: UnwrapRef<{
        password: string;
        email: string | undefined;
        username: string | undefined
    }>): Promise<T>
    getMe<T = any>(): Promise<T>
    getFlux<T = any>(): Promise<T>
    createFlux<T = any>(data: {name: string, flux_url: string, alert: string, alert_data: string, type: string, alert_array: string[]}): Promise<T>
    getFluxById<T = any>(id: string | RouteParamValue[]): Promise<T>
    getNewsByFlux<T = any>(id: string | RouteParamValue[]): Promise<T>
    getUsers<T = any>(): Promise<T>
    updateUser<T = any>(data: {username: string, email: string }): Promise<T>
    deleteUser<T = any>(): Promise<T>
    getNews<T = any>(): Promise<T>
    getNewsById<T = any>(id: string | RouteParamValue[]): Promise<T>
    createComments<T = any>(id: string, data: {text: string, id_comment_replay?: string}): Promise<T>
    getCommentsByNews<T = any>(id: string | RouteParamValue[]): Promise<T>
    postLike<T = any>(id: string): Promise<T>
    getFavorite<T = any>(): Promise<T>
    getTopLiked<T= any>(): Promise<T>
}
