import { defineNuxtPlugin } from '#app'
import type { Api } from '~/types/api'
import {useAuthStore} from "~/store/auth";

export default defineNuxtPlugin((nuxtApp) => {
    const baseUrl: String = 'http://localhost:3001'
    const token = useCookie('token')
    const user = useCookie('user')
    const authStore = useAuthStore()
    const api: Api = {
        async get<T = any>(url: string, options = {}): Promise<T> {
            const { data, error } = await useFetch(baseUrl + url, {
                ...options,
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${token.value}`
                }
            })
            if (error.value) {
                console.log('11111')
                console.log(error.value.data.statusCode === 403)
                if (error.value.data.statusCode === 403) {
                    console.log('ffff')
                    console.log(user.value)
                    await authStore.authenticateUser(user.value)
                }
            }
            return data.value as T
        },
        async post<T = any>(url: string, body: any, options = {}): Promise<T> {
            const { data, error } = await useFetch(baseUrl + url, {
                ...options,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body,
            })
            if (error.value) throw error.value
            return data.value as T
        },
        async postAuth<T = any>(url: string, body: any, options = {}): Promise<T> {
            const { data, error } = await useFetch(baseUrl + url, {
                ...options,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token.value}`
                },
                body,
            })
            if (error.value) throw error.value
            return data.value as T
        },
        async put<T = any>(url: string, body: any, options = {}): Promise<T> {
            const { data, error } = await useFetch(baseUrl + url, {
                ...options,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token.value}`
                },
                body,
            })
            if (error.value) throw error.value
            return data.value as T
        },
        async delete<T = any>(url: string, options = {}): Promise<T> {
            const { data, error } = await useFetch(baseUrl + url, {
                ...options,
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token.value}`
                }
            })
            if (error.value) throw error.value
            return data.value as T
        },
        async login<T = any>(data: { email: string; password: string }): Promise<T> {
            return await api.post<T>('/user/login', data)
        },
        async register<T = any>(data: {username: string, email: string, password: string}): Promise<T> {
            return await api.post<T>('/user/create', data)
        },
        async getMe<T = any>(): Promise<T> {
            return await api.get('/user/me')
        },
        async getFlux<T = any>(): Promise<T> {
            return await api.get('/flux')
        },
        async createFlux<T = any>(data: {name: string, flux_url: string, alert: string, alert_data: string, type: string, alert_array: string[]}): Promise<T> {
            return await api.postAuth('/flux/create', data)
        },
        async getFluxById<T = any>(id: string) {
            return await api.get('/flux/'+ id)
        },
        async getNewsByFlux<T = any>(id: string) {
            return await api.get('/news/flux/'+ id)
        },
        async getUsers<T = any>(): Promise<T> {
            return await api.get('/users')
        },
        async updateUser<T = any>(data: { username: string, email: string }){
            return await api.put('/user/me', data)
        },
        async deleteUser<T = any>() {
            return await api.delete('/user/me')
        },
        async getNews<T = any>() {
            return await api.get('/news')
        },
        async getNewsById< T = any>(id: string) {
            return await api.get('/news/' + id)
        },
        async createComments(id: string, data: {text: string, id_comment_replay?: string}){
            return await api.postAuth(`/${id}/comments/create`, data)
        },
        async getCommentsByNews(id: string) {
            return await api.get(`/${id}/comments`)
        },
        async postLike(id: string) {
            return await api.postAuth('/news/like/' + id, {})
        },
        async getFavorite() {
            return await api.get('/liked')
        },
        async getTopLiked() {
            return await api.get('/top-liked')
        }
    }

    nuxtApp.provide('api', api)
})
