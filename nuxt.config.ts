// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    app: {
        head: {
            title: 'DevNews',
            charset: 'utf-8',
            viewport: 'width=device-width, initial-scale=1',
            script: [
                {
                    src: 'https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.js',
                    tagPosition: 'bodyClose'
                }
            ]
        }
    },
  devtools: { enabled: true },
  modules: [
      "@nuxt/ui",
      "@pinia/nuxt"
  ],
  components: [
      '~/components/Header',
      '~/components/Footer',
      '~/components/Team',
      '~/components/Auth',
      '~/components/Auth/Form',
      '~/components/Global',
      '~/components/Flux',
      '~/components/Charts',
      '~/components/Feed',
      '~/components/Favorite',
  ],
    colorMode: {
        preference: 'light'
    },

    plugins: [
        '~/plugins/api.ts'
    ]
})