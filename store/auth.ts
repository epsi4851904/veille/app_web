import {defineStore} from 'pinia'
import {useCookie, useNuxtApp} from '#app'
import type {Api} from '~/types/api'
import type {integer} from "vscode-languageserver-types";
import bcrypt from 'bcryptjs'

interface UserPayloadInterface {
    email: string
    password: string
}

export const useAuthStore = defineStore('auth', {
    state: () => ({
        authenticated: false,
        loading: false,
        error: null as string | null,
        user: {}
    }),
    actions: {
        async authenticateUser({ email, password }: UserPayloadInterface) {
            const nuxtApp = useNuxtApp()
            const $api = nuxtApp.$api as Api

            try {
                this.loading = true
                this.error = null
                const data = await $api.login<{ Bearer: string, message: string, code: integer }>({ email, password })
                if (data.code !== 404) {
                    const token = useCookie('token')
                    const user = useCookie('user')
                    user.value = JSON.stringify({email, password})
                    token.value = data.Bearer
                    this.authenticated = true
                    await this.getUser()
                } else {
                    this.error = `L'email ou le mot de passe donné n'est pas bon`
                }
            } catch (error) {
                console.error('Error during authentication:', error)
                this.error = 'An error occurred during authentication'
            } finally {
                this.loading = false
            }
        },
        async logUserOut() {
            const router = useRouter();

            const token = useCookie('token')
            const user = useCookie('user')
            this.authenticated = false
            token.value = null
            user.value = null
            this.user = {}
            await router.push('/')
        },
        async getUser () {
            const nuxtApp = useNuxtApp()
            const $api = nuxtApp.$api as Api

            this.user = await $api.getMe()
            console.log(this.user)
        }
    },
})
